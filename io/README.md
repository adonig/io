io
==

Find all Google I/O patterns by DFS.

    IIIOOIII
    IIOIOOII
    IOOIOOOO
    IOOOOOOI
    OIIIIIII
    OIIIOIOI
    OIOIOOII
    OIOOOIOI
    OOIIIOOI
    OOIOIOIO