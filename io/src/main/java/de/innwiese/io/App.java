package de.innwiese.io;

public class App {

    public static void main(String[] args) {
        IOTreeDFS dfs = new IOTreeDFS();
        for (String pattern : dfs.getSuccessfulPatterns())
            System.out.println(pattern);
    }
}
