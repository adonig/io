package de.innwiese.io;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.google.common.collect.ImmutableSet;

public class IOTreeDFS {

    private static class Node {
        Node left;
        Node right;
        String pattern;
        String cssClass;

        Node(String pattern) {
            this.pattern = pattern;
        }
    }

    private final WebDriver driver;
    private final Set<String> successfulPatterns;

    public IOTreeDFS() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        successfulPatterns = new LinkedHashSet<String>();
        dfs(new Node(""));
        driver.quit();
    }

    private void dfs(Node n) {
        driver.get("https://developers.google.com/events/io/");
        WebElement canvas = driver.findElement(By.id("canvas-0"));
        canvas.sendKeys(n.pattern);
        WebElement pattern = driver.findElement(By.id("pattern"));
        n.cssClass = pattern.getAttribute("class");
        if (n.cssClass == null)
            n.cssClass = "";
        switch (n.cssClass) {
        case "success":
            successfulPatterns.add(n.pattern);
        case "failure":
            return;
        default:
            n.left = new Node(n.pattern + "I");
            n.right = new Node(n.pattern + "O");
            dfs(n.left);
            dfs(n.right);
        }
    }

    public Set<String> getSuccessfulPatterns() {
        return ImmutableSet.copyOf(successfulPatterns);
    }
}
